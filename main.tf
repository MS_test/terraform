provider "aws"{
	region = "us-west-2"
}

resource "aws_instance" "ec2" {
	ami = "test123"
	instance_type = lookup(var.instance_type,terraform.workspace,"t1.micro")
}
variable "instance_type"{
type = map
default = {
	default = "t1.micro"
	dev="t1.small"
	uat="t1.medium"
	prd = "t1.large"
}
}